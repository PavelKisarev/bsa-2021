import { createElement } from '../helpers/domHelper';


export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)
  const fighterName = createElement({
    tagName: 'p',
    className: 'fighter-preview___name'
  })
  fighterName.innerHTML = (fighter.name || '')

  const fighterImg = createFighterImage(fighter)

  const fighterParams = createElement({
    tagName: 'div',
    className: 'fighter-preview___params'
  })

  fighterParams.innerHTML = `
                            <p>Health - ${fighter.health}</p>
                            <p>Attack - ${fighter.attack}</p>
                            <p>Defense - ${fighter.defense}</p>
                            `

  fighterElement.append(fighterName, fighterImg, fighterParams)

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}




