import { controls } from '../../constants/controls';

let firstFighterTactic = '', secondFighterTactic = ''

export async function fight(firstFighter, secondFighter) {
  console.log('firstFighter =', firstFighter)
  console.log('secondFighter =', secondFighter)

  // let firstFighterTactic = '', secondFighterTactic = ''
  let selectedTactics = []

  let healthF = firstFighter.health
  let healthS = secondFighter.health

  let winner


  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over

    let canFirstCrit = true, canSecondCrit = true

    checkPressedCombination(
      () => {
        if (canFirstCrit) {
          firstFighterTactic = getKeyByValue(controls, ['KeyQ', 'KeyW', 'KeyE'])
          console.log('COMB', firstFighterTactic)
          canFirstCrit = false
          setTimeout(() => {
            console.log('Change canFirstCrit')
            canFirstCrit = true
          }, 10000);
        }
        else { console.log('WAIT! Can not crit now') }
      },
      'KeyQ',
      'KeyW',
      'KeyE'
    )

    checkPressedCombination(
      () => {
        if (canSecondCrit) {
          secondFighterTactic = getKeyByValue(controls, ['KeyU', 'KeyI', 'KeyO'])
          console.log('COMB2', secondFighterTactic)
          canSecondCrit = false
          setTimeout(() => {
            console.log('Change canSecondCrit')
            canSecondCrit = true
          }, 10000);
        }
        else { console.log('WAIT! Can not crit now') }
      },
      'KeyU',
      'KeyI',
      'KeyO'
    )

    checkSimplePress()


    document.addEventListener('keydown', function fightingProcess(e) {

      selectedTactics = [firstFighterTactic, secondFighterTactic]

      console.log('selectedTactics', selectedTactics)
      const canCountDamage = selectedTactics.filter(Boolean).length === 2;

      if (canCountDamage) {

        let plan = fightRound(firstFighterTactic, secondFighterTactic)
        switch (plan) {
          case 'BlockWithoutDamage':
            console.log('curSwitch', plan)
            resetTactics()
            break
          case 'AllHitBasic':
            console.log('curSwitch', plan)
            basicFight(null)
            break
          case 'AllHitCrit':
            console.log('curSwitch', plan)
            basicFight('AllHitCrit')
            break
          case 'FirstCritSecondBlock':
            console.log('curSwitch', plan)
            basicFight('FirstCritSecondBlock')
            break
          case 'FirstCritSecondHit':
            console.log('curSwitch', plan)
            basicFight('FirstCritSecondHit')
            break
          case 'SecondCritFirstBlock':
            console.log('curSwitch', plan)
            basicFight('SecondCritFirstBlock')
            break
          case 'SecondCritFirstHit':
            console.log('curSwitch', plan)
            basicFight('SecondCritFirstHit')
            break
        }
      }


      function basicFight(whoCrit) {

        switch (whoCrit) {
          case 'AllCrit':
            healthF -= getCritDamage(secondFighter)
            healthS -= getCritDamage(firstFighter)
            break
          case 'FirstCritSecondBlock':
            healthS -= getCritDamage(firstFighter)
            break
          case 'FirstCritSecondHit':
            healthF -= getDamage(secondFighter, firstFighter)
            healthS -= getCritDamage(firstFighter)
            break
          case 'SecondCritFirstBlock':
            healthF -= getCritDamage(secondFighter)
            break
          case 'SecondCritFirstHit':
            healthF -= getCritDamage(secondFighter)
            healthS -= getDamage(firstFighter, secondFighter)
            break
          default:
            healthF -= getDamage(secondFighter, firstFighter)
            healthS -= getDamage(firstFighter, secondFighter)
        }

        console.log('cur health first fighter =', healthF)
        console.log('cur health second fighter =', healthS)

        if (healthF <= 0) {
          resolve(secondFighter)
        }

        if (healthS <= 0) {
          resolve(firstFighter)
        }

        changeHealthBar(healthF, firstFighter.health, 1)
        changeHealthBar(healthS, secondFighter.health, 2)

        resetTactics()
      }

    })

  });
}

export function getDamage(attacker, defender) {
  let damage = getHitPower(attacker) - getBlockPower(defender)
  damage = damage < 0 ? 0 : damage
  return damage
}

export function getHitPower(fighter) {
  return fighter.attack * (Math.random() + 1)
}

export function getBlockPower(fighter) {
  return fighter.defense * (Math.random() + 1)
}

// calculate crit damage
function getCritDamage(fighter) {
  return fighter.attack * 2
}

// calculate total fighers health
function changeHealthBar(curHealth, totalHealth, playerNumber) {
  const healthBar = document.querySelectorAll('.arena___health-bar')[playerNumber - 1]

  if (curHealth > 0) {
    let curHealthBarWidth = Math.round(curHealth * 100 / totalHealth)
    healthBar.style.width = curHealthBarWidth + '%'
  }
  else {
    healthBar.style.width = 0 + '%'
    document.querySelectorAll('.arena___fighter')[playerNumber - 1].classList.add('loose-game')
    console.log("GAME OVER")
    document.removeEventListener('keydown', fightingProcess)
  }


}

// get keys from controls objects by their values
function getKeyByValue(obj, value) {
  if (Array.isArray(value)) {
    return Object.keys(obj).find(key => compareArrs(obj[key], value))
  }
  else {
    return Object.keys(obj).find(key => obj[key] === value)
  }
}

// check basic attack and block (press A D J L)
function checkSimplePress() {
  document.addEventListener('keydown', (e) => {
    if (e.code === 'KeyA' || e.code === 'KeyD') {
      firstFighterTactic = getKeyByValue(controls, e.code)
    }

    if (e.code === 'KeyJ' || e.code === 'KeyL') {
      secondFighterTactic = getKeyByValue(controls, e.code)
    }
  })
}

// check critical combination
function checkPressedCombination(func, ...keyCodes) {
  let pressed = new Set()

  document.addEventListener('keydown', function (e) {
    pressed.add(e.code)
    for (let curCode of keyCodes) {
      if (!pressed.has(curCode)) {
        return
      }
    }

    pressed.clear()
    func()
  })

  document.addEventListener('keyup', function (e) {
    pressed.delete(e.code);
  });
}

// compare two arrays
function compareArrs(arr1, arr2) {
  return arr1.length == arr2.length && arr1.every((f, s) => f === arr2[s])
}

// reset tactics after hit or block
function resetTactics() {
  firstFighterTactic = ''
  secondFighterTactic = ''
}


// choose fighters fight plan
function fightRound(firstFighterTactic, secondFighterTactic) {
  let fightPlan = ''

  if (firstFighterTactic === getKeyByValue(controls, 'KeyD')
    && secondFighterTactic === getKeyByValue(controls, 'KeyJ')) {
    console.log("Second Attack First block")
    fightPlan = 'BlockWithoutDamage'
  }
  if (firstFighterTactic === getKeyByValue(controls, 'KeyA')
    && secondFighterTactic === getKeyByValue(controls, 'KeyL')) {
    console.log("First Attack Second block")
    fightPlan = 'BlockWithoutDamage'
  }

  if (firstFighterTactic === getKeyByValue(controls, 'KeyD')
    && secondFighterTactic === getKeyByValue(controls, 'KeyL')) {
    console.log("All BLOCK")
    fightPlan = 'BlockWithoutDamage'
  }

  if (firstFighterTactic === getKeyByValue(controls, 'KeyA')
    && secondFighterTactic === getKeyByValue(controls, 'KeyJ')) {
    console.log("AllHitBasic")
    fightPlan = 'AllHitBasic'
  }

  if (firstFighterTactic === getKeyByValue(controls, ['KeyQ', 'KeyW', 'KeyE'])
    && secondFighterTactic === getKeyByValue(controls, ['KeyU', 'KeyI', 'KeyO'])) {
    console.log("AllHitCrit")
    fightPlan = 'AllHitCrit'
  }

  if (firstFighterTactic === getKeyByValue(controls, ['KeyQ', 'KeyW', 'KeyE'])) {
    if (secondFighterTactic === getKeyByValue(controls, 'KeyL')) {
      console.log('FirstCritSecondBlock')
      fightPlan = 'FirstCritSecondBlock'
    }
    if (secondFighterTactic === getKeyByValue(controls, 'KeyJ')) {
      console.log('FirstCritSecondHit')
      fightPlan = 'FirstCritSecondHit'
    }
  }

  if (secondFighterTactic === getKeyByValue(controls, ['KeyU', 'KeyI', 'KeyO'])) {
    if (firstFighterTactic === getKeyByValue(controls, 'KeyD')) {
      console.log('SecondCritFirstBlock')
      fightPlan = 'SecondCritFirstBlock'
    }
    if (firstFighterTactic === getKeyByValue(controls, 'KeyA')) {
      console.log('SecondCritFirstHit')
      fightPlan = 'SecondCritFirstHit'
    }
  }

  return fightPlan
}





