import { showModal } from './modal'

export function showWinnerModal(fighter) {
  // call showModal function 
  showModal({
    title: `The winner of this fight is ${fighter.name}`,
    bodyElement: 'Congratulations',
    onClose: () => {
      console.log('CLOSE MODAL AND RESTART')
      location.reload()
    }
  })
}

